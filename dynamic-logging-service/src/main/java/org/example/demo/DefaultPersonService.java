package org.example.demo;

import java.util.HashMap;
import java.util.Map;

public class DefaultPersonService implements PersonService {

    private final Map<Long, Person> personMap = new HashMap<>();
    private long seqNumber = 1;

    @Override
    public Person getPerson(Long personId) {
        return personMap.get(personId);
    }

    @Override
    public Long setPerson(String firstName, String lastName) {
        Long id = seqNumber++;
        Person person = new Person(id, firstName, lastName);
        personMap.put(id, person);
        return id;
    }

    @Override
    public void deletePerson(Long personId) {
        personMap.remove(personId);
    }
}
