package org.example.demo;

public interface PersonService {

    public Person getPerson(Long personId);

    public Long setPerson(String firstName, String lastName);

    public void deletePerson(Long personId);
}
