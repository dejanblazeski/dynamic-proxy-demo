package org.example.demo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class LoggingInvocationHandler implements InvocationHandler {

    private final PersonService personService;

    public LoggingInvocationHandler(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("Method being called " + method.getName());
        Object result = method.invoke(this.personService, args);
        System.out.println("Method finished " + method.getName());
        return result;
    }
}
