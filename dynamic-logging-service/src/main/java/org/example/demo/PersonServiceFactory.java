package org.example.demo;

import java.lang.reflect.Proxy;

public class PersonServiceFactory {

    private PersonServiceFactory() {
    }

    public static PersonService getLoggingInstance(PersonService personService) {
        return (PersonService) Proxy.newProxyInstance(PersonServiceFactory.class.getClassLoader(),
                new Class[]{PersonService.class},
                new LoggingInvocationHandler(personService));
    }
}
