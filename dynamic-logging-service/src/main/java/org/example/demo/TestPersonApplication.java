package org.example.demo;

public class TestPersonApplication {

    public static void main(String[] args) {
        PersonService personService = PersonServiceFactory.getLoggingInstance(new DefaultPersonService());
        Long id = personService.setPerson("First", "Last");
        Person person = personService.getPerson(id);
        personService.deletePerson(id);
    }
}
