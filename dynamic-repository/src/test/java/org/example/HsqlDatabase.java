package org.example;

import org.hsqldb.cmdline.SqlFile;

import java.nio.file.Paths;
import java.sql.Connection;


import java.sql.DriverManager;
import java.sql.SQLException;

public class HsqlDatabase {
    private static final String CONNECTION_STRING = "jdbc:hsqldb:mem:testdb;shutdown=false";
    private static final String USER_NAME = "SA";
    private static final String PASSWORD = "";

    public void setUp() {
        try {
            Class.forName("org.hsqldb.jdbcDriver");

            SqlFile sf = new SqlFile(Paths.get(this.getClass().getResource("/schema.sql").toURI()).toFile());
            sf.setConnection(getConnectionImpl());
            sf.execute();

        } catch (Exception ex) {
            throw new RuntimeException("Error during database initialization", ex);
        }
    }

    public Connection getConnectionImpl() throws SQLException {
        return DriverManager.getConnection(CONNECTION_STRING, USER_NAME, PASSWORD);
    }
}