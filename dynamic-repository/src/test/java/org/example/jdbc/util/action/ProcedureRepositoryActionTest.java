package org.example.jdbc.util.action;

import org.example.jdbc.util.RepositoryResult;
import org.example.jdbc.util.annotation.Procedure;
import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.lang.annotation.Annotation;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ProcedureRepositoryActionTest {

    private static final String procedureName = "TEST_PROCEDURE";

    private static final String procedureResultParameter = "O_RESULT";

    private ProcedureRepositoryAction procedureRepositoryAction;

    private RepositoryMethodMetadata repositoryMethodVoidMetadata;

    private RepositoryMethodMetadata repositoryMethodNonVoidMetadata;

    private Connection connection;

    private CallableStatement callableStatement;

    private Annotation procedure = new Procedure() {
        @Override
        public Class<? extends Annotation> annotationType() {
            return Procedure.class;
        }

        @Override
        public String procedureName() {
            return procedureName;
        }

        @Override
        public String resultParameterName() {
            return procedureResultParameter;
        }
    };

    @BeforeEach
    public void setUp() {
        this.connection = Mockito.mock(Connection.class);
        this.callableStatement = Mockito.mock(CallableStatement.class);
        this.repositoryMethodVoidMetadata = new RepositoryMethodMetadata(new ArrayList<>(), void.class, procedure);
        this.repositoryMethodNonVoidMetadata = new RepositoryMethodMetadata(new ArrayList<>(), String.class, procedure);
        this.procedureRepositoryAction = new ProcedureRepositoryAction();
    }

    @Test
    public void whenProcedureRepositoryActionCreated_thenSqlCall() {
        String sqlCall = this.procedureRepositoryAction.getRepositoryCall(this.repositoryMethodVoidMetadata);
        assertEquals("{ CALL TEST_PROCEDURE()}", sqlCall);
    }

    @Test
    public void whenMethodIsAnnotatedWithProcedure_thenActionIsSupported() {
        Boolean isSupported = this.procedureRepositoryAction.isSupported(procedure);
        assertTrue(isSupported);
    }

    @Test
    public void whenMethodIsNotAnnotatedWithProcedure_thenActionIsNotSupported() {
        Annotation annotation = new Override() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return Override.class;
            }
        };
        Boolean isSupported = this.procedureRepositoryAction.isSupported(annotation);
        assertFalse(isSupported);
    }

    @Test
    public void whenMethodIsNotAnnotatedWithAnyAnnotation_thenActionIsNotSupported() {
        Boolean isSupported = this.procedureRepositoryAction.isSupported(null);
        assertFalse(isSupported);
    }

    @Test
    public void whenProcedureRepositoryWithoutResult_thenRepositoryResultVoid() throws SQLException {
        Mockito.when(connection.prepareCall(Mockito.anyString())).thenReturn(this.callableStatement);
        RepositoryResult result = this.procedureRepositoryAction.execute(connection, this.repositoryMethodVoidMetadata, new ArrayList<>());
        assertEquals(result, new RepositoryResult(void.class, null));
    }

    @Test
    public void whenProcedureRepositoryWithResult_thenRepositoryResultNonVoid() throws SQLException {
        Mockito.when(connection.prepareCall(Mockito.anyString())).thenReturn(this.callableStatement);
        Mockito.when(this.callableStatement.getObject(procedureResultParameter)).thenReturn("TEST DATA");

        RepositoryResult result = this.procedureRepositoryAction.execute(connection, this.repositoryMethodNonVoidMetadata, new ArrayList<>());
        assertEquals(result, new RepositoryResult(String.class, "TEST DATA"));
    }

    @Test
    public void whenProcedureRepositoryCalledWithException_thenThrowException() throws Exception {
        Mockito.when(connection.prepareCall(Mockito.anyString())).thenThrow(new SQLException());
        assertThrows(RuntimeException.class, () -> {
            this.procedureRepositoryAction.execute(connection, this.repositoryMethodVoidMetadata, new ArrayList<>());
        });
    }
}