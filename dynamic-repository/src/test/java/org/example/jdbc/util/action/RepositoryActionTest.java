package org.example.jdbc.util.action;

import org.example.jdbc.util.RepositoryResult;
import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryActionTest {

    private RepositoryAction repositoryAction;

    @BeforeEach
    public void setUp() {
        this.repositoryAction = new RepositoryAction() {
            @Override
            public boolean isSupported(Annotation annotation) {
                return false;
            }

            @Override
            public String getRepositoryCall(RepositoryMethodMetadata repositoryMethodMetadata) {
                return null;
            }

            @Override
            public RepositoryResult execute(Connection connection, RepositoryMethodMetadata repositoryMethodMetadata, List<RepositoryMethodPropertyValue> propertyValues) {
                return null;
            }
        };
    }

    @Test
    public void whenCreatingCallParamsWithZeroElements_thenEmptyString() {
        String callParams = this.repositoryAction.getRepositoryCallParams(new ArrayList<>());
        assertEquals("", callParams);
    }

    @Test
    public void whenCreatingCallParamsWithMultipleElements_thenStringWithQuestionmarks() {
        List<RepositoryMethodPropertyMetadata> methodProperties = new ArrayList<>();
        RepositoryMethodPropertyMetadata repositoryMethodPropertyMetadata = new RepositoryMethodPropertyMetadata("property1",1, String.class);
        RepositoryMethodPropertyMetadata repositoryMethodPropertyMetadata2 = new RepositoryMethodPropertyMetadata("property2",2, String.class);
        RepositoryMethodPropertyMetadata repositoryMethodPropertyMetadata3 = new RepositoryMethodPropertyMetadata("property3", 3, String.class);
        methodProperties.add(repositoryMethodPropertyMetadata);
        methodProperties.add(repositoryMethodPropertyMetadata2);
        methodProperties.add(repositoryMethodPropertyMetadata3);
        String callParams = this.repositoryAction.getRepositoryCallParams(methodProperties);
        assertEquals("?,?,?", callParams);
    }

}