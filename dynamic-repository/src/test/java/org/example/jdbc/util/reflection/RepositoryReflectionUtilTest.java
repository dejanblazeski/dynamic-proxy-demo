package org.example.jdbc.util.reflection;

import org.example.jdbc.util.annotation.Param;
import org.example.jdbc.util.annotation.Procedure;
import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyMetadata;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryReflectionUtilTest {

    private interface TestRepositoryInterface {

        public Integer testMethod();

        @Procedure(procedureName = "TEST_PROCEDURE", resultParameterName = "O_RESULT")
        public String secondMethod(@Param(name = "first") String first, @Param(name = "last") String last);

        public String thirdMethod(String first, String last);

        @Procedure(procedureName = "ANOTHER_TEST_PROCEDURE", resultParameterName = "O_RESULT")
        public String forthMethod(@Param(name = "first") String first, String last);
    }

    @Test
    public void whenMethodIsRepositoryAnnotation_thenReturnsAnnotation() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("secondMethod", String.class, String.class);
        Annotation annotation = RepositoryReflectionUtil.getRepositoryAnnotation(method);
        assertEquals(Procedure.class, annotation.annotationType());
    }

    @Test
    public void whenMethodIsNotRepositoryAnnotation_thenReturnNull() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("testMethod");
        Annotation annotation = RepositoryReflectionUtil.getRepositoryAnnotation(method);
        assertNull(annotation);
    }

    @Test
    public void whenMethodParameterIsParamAnnotation_thenReturnAnnotation() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("secondMethod", String.class, String.class);
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        Annotation paramAnnotation = RepositoryReflectionUtil.getParamAnnotation(parameterAnnotations[0]);
        assertEquals(Param.class, paramAnnotation.annotationType());
    }

    @Test
    public void whenMethodParameterIsNotParamAnnotation_thenReturnNull() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("thirdMethod", String.class, String.class);
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        Annotation paramAnnotation = RepositoryReflectionUtil.getParamAnnotation(parameterAnnotations[0]);
        assertNull(paramAnnotation);
    }

    @Test
    public void whenMethodIsRepositoryMethod_thenReturnMetadata() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("secondMethod", String.class, String.class);
        RepositoryMethodMetadata repositoryMethodMetadata = RepositoryReflectionUtil.getRepositoryMethodMetadata(method);
        assertNotNull(repositoryMethodMetadata);
        assertEquals(method.getReturnType(), repositoryMethodMetadata.getResultClass());
        assertEquals(Procedure.class, repositoryMethodMetadata.getRepositoryAnnotation().annotationType());
        assertEquals(2, repositoryMethodMetadata.getProperties().size());
    }

    @Test
    public void whenMethodIsNotRepositoryMethod_thenReturnNull() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("thirdMethod", String.class, String.class);
        RepositoryMethodMetadata methodMetadata = RepositoryReflectionUtil.getRepositoryMethodMetadata(method);
        assertNull(methodMetadata);
    }

    @Test
    public void whenMethodIsRepositoryAndAllParametersAnnotatedWithParam_thenReturnPropertiesMetadata() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("secondMethod", String.class, String.class);
        List<RepositoryMethodPropertyMetadata> properties = RepositoryReflectionUtil.getProperties(method);
        assertEquals(2, properties.size());
    }

    @Test
    public void whenMethodIsRepositoryAndOneParameterNotAnnotatedWithParams_thenThrowException() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("forthMethod", String.class, String.class);
        assertThrows(IllegalArgumentException.class, () -> {
            RepositoryReflectionUtil.getProperties(method);
        });
    }

    @Test
    public void whenMethodIsNotRepositoryAndNoParameterHasAnnotation_thenThrowException() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("thirdMethod", String.class, String.class);
        assertThrows(IllegalArgumentException.class, () -> {
            RepositoryReflectionUtil.getProperties(method);
        });
    }

    @ParameterizedTest
    @MethodSource("getRepositoryMethodParams")
    public void whenFirstParameterIsParamAnnotation_thenReturnParamMetadata(String methodName, Class parameterType, Annotation[] parameterAnnotations, Integer parameterIndex, String paramName) {
        RepositoryMethodPropertyMetadata repositoryMethodPropertyMetadata = RepositoryReflectionUtil.getRepositoryMethodPropertyMetadata(methodName, parameterType, parameterAnnotations, parameterIndex);
        assertEquals(parameterType, repositoryMethodPropertyMetadata.getPropertyType());
        assertEquals(parameterIndex + 1, repositoryMethodPropertyMetadata.getPropertyIndex());
        assertEquals(paramName, repositoryMethodPropertyMetadata.getPropertyName());
    }

    @Test
    public void whenParameterIsNotParamAnnotation_thenThrowException() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("thirdMethod", String.class, String.class);
        assertThrows(IllegalArgumentException.class, () -> {
            RepositoryReflectionUtil.getRepositoryMethodPropertyMetadata(method.getName(), method.getParameterTypes()[0], method.getParameterAnnotations()[0], 0);
        });

    }

    private static Stream<Arguments> getRepositoryMethodParams() throws NoSuchMethodException {
        Method method = TestRepositoryInterface.class.getMethod("secondMethod", String.class, String.class);
        final String methodName = method.getName();
        List<Arguments> arguments = new ArrayList<>();
        for (int i = 0; i < method.getParameterTypes().length; i++) {
            Annotation[] paramAnnotations = method.getParameterAnnotations()[i];
            Param param = (Param) RepositoryReflectionUtil.getParamAnnotation(paramAnnotations);
            arguments.add(Arguments.of(methodName, method.getParameterTypes()[i], paramAnnotations, i, param != null ? param.name() : null));
        }
        return arguments.stream();
    }
}