package org.example.jdbc.util.action;

import org.example.jdbc.util.RepositoryResult;
import org.example.jdbc.util.annotation.Function;
import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.lang.annotation.Annotation;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class FunctionRepositoryActionTest {

    private static final String functionName = "TEST_FUNCTION";

    private FunctionRepositoryAction functionRepositoryAction;

    private RepositoryMethodMetadata repositoryMethodVoidMetadata;

    private RepositoryMethodMetadata repositoryMethodNonVoidMetadata;

    private Connection connection;

    private CallableStatement callableStatement;

    private Annotation function = new Function() {
        @Override
        public String value() {
            return functionName;
        }

        @Override
        public String resultParameterName() {
            return "";
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return Function.class;
        }
    };

    @BeforeEach
    public void setUp() {
        this.connection = Mockito.mock(Connection.class);
        this.callableStatement = Mockito.mock(CallableStatement.class);
        this.repositoryMethodVoidMetadata = new RepositoryMethodMetadata(new ArrayList<>(), void.class, function);
        this.repositoryMethodNonVoidMetadata = new RepositoryMethodMetadata(new ArrayList<>(), String.class, function);
        this.functionRepositoryAction = new FunctionRepositoryAction();
    }

    @Test
    public void whenFunctionRepositoryActionCreated_thenSqlCall() {
        String sqlCall = this.functionRepositoryAction.getRepositoryCall(this.repositoryMethodVoidMetadata);
        assertEquals("{? = CALL TEST_FUNCTION()}", sqlCall);
    }

    @Test
    public void whenMethodIsAnnotatedWithFunction_thenActionIsSupported() {
        Boolean isSupported = this.functionRepositoryAction.isSupported(function);
        assertTrue(isSupported);
    }

    @Test
    public void whenMethodIsNotAnnotatedWithFunction_thenActionIsNotSupported() {
        Annotation annotation = new Override() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return Override.class;
            }
        };
        Boolean isSupported = this.functionRepositoryAction.isSupported(annotation);
        assertFalse(isSupported);
    }

    @Test
    public void whenMethodIsNotAnnotatedWithAnyAnnotation_thenActionIsNotSupported() {
        Boolean isSupported = this.functionRepositoryAction.isSupported(null);
        assertFalse(isSupported);
    }

    @Test
    public void whenFunctionRepositoryWithoutResult_thenRepositoryResultVoid() throws SQLException {
        Mockito.when(connection.prepareCall(Mockito.anyString())).thenReturn(this.callableStatement);
        RepositoryResult result = this.functionRepositoryAction.execute(connection, this.repositoryMethodVoidMetadata, new ArrayList<>());
        assertEquals(result, new RepositoryResult(void.class, null));
    }

    @Test
    public void whenFunctionRepositoryWithResult_thenRepositoryResultNonVoid() throws SQLException {
        Mockito.when(connection.prepareCall(Mockito.anyString())).thenReturn(this.callableStatement);
        Mockito.when(this.callableStatement.getObject(1)).thenReturn("TEST DATA");

        RepositoryResult result = this.functionRepositoryAction.execute(connection, this.repositoryMethodNonVoidMetadata, new ArrayList<>());
        assertEquals(result, new RepositoryResult(String.class, "TEST DATA"));
    }

    @Test
    public void whenFunctionRepositoryCalledWithException_thenThrowException() throws Exception {
        Mockito.when(connection.prepareCall(Mockito.anyString())).thenThrow(new SQLException());
        assertThrows(RuntimeException.class, () -> {
            this.functionRepositoryAction.execute(connection, this.repositoryMethodVoidMetadata, new ArrayList<>());
        });
    }

}