package org.example.jdbc.util.action;

import org.example.HsqlDatabase;
import org.example.jdbc.util.DataSourceUtil;
import org.example.jdbc.util.RepositoryFactory;
import org.example.jdbc.util.TestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import javax.sql.DataSource;

public class TestRepositoryTest {

    private static HsqlDatabase hsqlDatabase = new HsqlDatabase();

    @BeforeAll
    public static void setUp() {
        hsqlDatabase.setUp();
    }

    @Test
    public void testProcedureAnnotation() throws Exception {
        DataSource dataSource = Mockito.mock(DataSource.class);
        try (MockedStatic<DataSourceUtil> dataSourceUtil = Mockito.mockStatic(DataSourceUtil.class)) {
            dataSourceUtil.when(DataSourceUtil::getDataSource).thenReturn(dataSource);
            Mockito.when(dataSource.getConnection()).thenReturn(hsqlDatabase.getConnectionImpl());
            TestRepository testRepository = RepositoryFactory.getInstance(TestRepository.class);
            String fullName = testRepository.getFullName("TEST", "NAME");
            Assertions.assertEquals("TESTNAME", fullName);
        }
    }

    @Test
    public void anotherTest() throws Exception {
        DataSource dataSource = Mockito.mock(DataSource.class);
        try (MockedStatic<DataSourceUtil> dataSourceUtil = Mockito.mockStatic(DataSourceUtil.class)) {
            dataSourceUtil.when(DataSourceUtil::getDataSource).thenReturn(dataSource);
            Mockito.when(dataSource.getConnection()).thenReturn(hsqlDatabase.getConnectionImpl());
            TestRepository testRepository = RepositoryFactory.getInstance(TestRepository.class);
            testRepository.testProcedure("First");
        }
    }

    @Test
    public void testQueryAnnotation() throws Exception {
        DataSource dataSource = Mockito.mock(DataSource.class);
        try (MockedStatic<DataSourceUtil> dataSourceUtil = Mockito.mockStatic(DataSourceUtil.class)) {
            dataSourceUtil.when(DataSourceUtil::getDataSource).thenReturn(dataSource);
            Mockito.when(dataSource.getConnection()).thenReturn(hsqlDatabase.getConnectionImpl());
            TestRepository testRepository = RepositoryFactory.getInstance(TestRepository.class);
            String firstName = testRepository.getFirstName(1);
            Assertions.assertEquals("First", firstName);
        }
    }

    @Test
    public void testNonRepositoryNonMethod() throws Exception {
        DataSource dataSource = Mockito.mock(DataSource.class);
        try (MockedStatic<DataSourceUtil> dataSourceUtil = Mockito.mockStatic(DataSourceUtil.class)) {
            dataSourceUtil.when(DataSourceUtil::getDataSource).thenReturn(dataSource);
            Mockito.when(dataSource.getConnection()).thenReturn(hsqlDatabase.getConnectionImpl());
            TestRepository testRepository = RepositoryFactory.getInstance(TestRepository.class);
            String firstName = testRepository.nonRepositoryNonVoidMethod();
            Assertions.assertNull(firstName);
        }
    }

    @Test
    public void testNonRepositoryVoidMethod() throws Exception {
        DataSource dataSource = Mockito.mock(DataSource.class);
        try (MockedStatic<DataSourceUtil> dataSourceUtil = Mockito.mockStatic(DataSourceUtil.class)) {
            dataSourceUtil.when(DataSourceUtil::getDataSource).thenReturn(dataSource);
            Mockito.when(dataSource.getConnection()).thenReturn(hsqlDatabase.getConnectionImpl());
            TestRepository testRepository = RepositoryFactory.getInstance(TestRepository.class);
            testRepository.nonRepositoryVoidMethod("string");
        }
    }
}
