package org.example.jdbc.util;

import org.example.jdbc.util.annotation.Function;
import org.example.jdbc.util.annotation.Param;
import org.example.jdbc.util.annotation.Procedure;
import org.example.jdbc.util.annotation.Query;

public interface TestRepository extends Repository {

    @Procedure(procedureName = "full_name", resultParameterName = "fullname")
    String getFullName(@Param(name = "firstname") String firstName, @Param(name = "lastname") String lastname);

    @Procedure(procedureName = "test_procedure")
    void testProcedure(@Param(name = "firstname") String firstName);

    @Function("test_function")
    Integer testFunction();

    @Query("select first_name from person where id = ?")
    String getFirstName(@Param(name = "id") Integer id);

    String nonRepositoryNonVoidMethod();

    void nonRepositoryVoidMethod(String parameter);
}
