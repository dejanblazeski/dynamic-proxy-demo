package org.example.jdbc.util.metadata;

public enum JDBCType {
    NUMERIC(Number.class, java.sql.JDBCType.NUMERIC),
    VARCHAR(String.class, java.sql.JDBCType.VARCHAR),
    CHAR(Character.class, java.sql.JDBCType.CHAR);

    private Class clazz;

    private java.sql.JDBCType jdbcType;

    JDBCType(Class clazz, java.sql.JDBCType jdbcType) {
        this.clazz = clazz;
        this.jdbcType = jdbcType;
    }

    public Class getClazz() {
        return clazz;
    }

    public java.sql.JDBCType getJdbcType() {
        return jdbcType;
    }

    public static JDBCType getFromClass(Class clazz) {
        for (JDBCType jdbcType : JDBCType.values()) {
            if (jdbcType.clazz.isAssignableFrom(clazz)) {
                return jdbcType;
            }
        }
        throw new RuntimeException("Unknown JDBC type for class " + clazz.getSimpleName());
    }
}
