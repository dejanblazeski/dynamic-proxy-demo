package org.example.jdbc.util;

import java.util.Objects;

/**
 * Result that is retrieved from {@link org.example.jdbc.util.action.RepositoryAction#execute}
 * It holds the actual object of the result and class type.
 */
public class RepositoryResult {

    private final Class resultClass;
    private final Object result;

    public RepositoryResult(Class resultClass, Object result) {
        this.resultClass = resultClass;
        this.result = result;
    }

    /**
     * Check if result is present.
     * Result is considered any class type that is different than void.class
     *
     * @return true if result type is not void.class, otherwise false
     */
    public boolean hasResult() {
        return !void.class.isAssignableFrom(resultClass);
    }

    public Class getResultClass() {
        return resultClass;
    }

    public Object getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RepositoryResult result1 = (RepositoryResult) o;
        return Objects.equals(resultClass, result1.resultClass) && Objects.equals(result, result1.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resultClass, result);
    }
}
