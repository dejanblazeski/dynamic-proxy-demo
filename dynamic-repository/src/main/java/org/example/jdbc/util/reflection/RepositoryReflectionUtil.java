package org.example.jdbc.util.reflection;

import org.example.jdbc.util.annotation.Function;
import org.example.jdbc.util.annotation.Param;
import org.example.jdbc.util.annotation.Procedure;
import org.example.jdbc.util.annotation.Query;
import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyMetadata;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RepositoryReflectionUtil {

    private static final List<Class> repositoryAnnotations = Arrays.asList(Procedure.class, Function.class, Query.class);


    private RepositoryReflectionUtil() {
    }

    public static List<RepositoryMethodPropertyMetadata> getProperties(Method method) {
        List<RepositoryMethodPropertyMetadata> properties = new ArrayList<>();
        Class[] parameters = method.getParameterTypes();
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (int i = 0; i < parameters.length; i++) {
            Class parameter = parameters[i];
            RepositoryMethodPropertyMetadata propertyMetadata = getRepositoryMethodPropertyMetadata(method.getName(), parameter, parameterAnnotations[i], i);
            properties.add(propertyMetadata);
        }
        return properties;
    }

    static RepositoryMethodPropertyMetadata getRepositoryMethodPropertyMetadata(String methodName, Class parameterType, Annotation[] parameterAnnotations, Integer parameterIndex) {
        Param param = (Param) getParamAnnotation(parameterAnnotations);
        if (param == null) {
            throw new IllegalArgumentException("Method " + methodName + " is annotated with one of the repository annotations. @Param must be added on every method argument");
        }
        return new RepositoryMethodPropertyMetadata(param.name(), parameterIndex + 1, parameterType);
    }

    static Annotation getParamAnnotation(Annotation[] parameterAnnotations) {
        for (Annotation annotation : parameterAnnotations) {
            if (annotation.annotationType() == Param.class) {
                return annotation;
            }
        }
        return null;
    }

    public static Annotation getRepositoryAnnotation(Method method) {
        for (Class repositoryAnnotation : repositoryAnnotations) {
            Annotation annotation = method.getAnnotation(repositoryAnnotation);
            if (annotation != null) {
                return annotation;
            }
        }
        return null;
    }

    public static RepositoryMethodMetadata getRepositoryMethodMetadata(Method method) {
        Annotation repositoryAnnotation = RepositoryReflectionUtil.getRepositoryAnnotation(method);
        if (repositoryAnnotation == null) {
            return null;
        }
        Class returnType = method.getReturnType();
        List<RepositoryMethodPropertyMetadata> properties = RepositoryReflectionUtil.getProperties(method);
        return new RepositoryMethodMetadata(properties, returnType, repositoryAnnotation);
    }
}
