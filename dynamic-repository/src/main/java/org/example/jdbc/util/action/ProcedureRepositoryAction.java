package org.example.jdbc.util.action;

import org.example.jdbc.util.RepositoryResult;
import org.example.jdbc.util.annotation.Procedure;
import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyValue;

import java.lang.annotation.Annotation;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.List;

public class ProcedureRepositoryAction implements RepositoryAction {

    public ProcedureRepositoryAction() {
    }

    @Override
    public boolean isSupported(Annotation annotation) {
        return annotation != null && Procedure.class == annotation.annotationType();
    }

    @Override
    public String getRepositoryCall(RepositoryMethodMetadata repositoryMethodMetadata) {
        StringBuilder sb = new StringBuilder()
                .append("{")
                .append(" CALL ")
                .append(repositoryMethodMetadata.unwrap(Procedure.class).procedureName())
                .append("(")
                .append(this.getRepositoryCallParams(repositoryMethodMetadata.getProperties()));
        if (!repositoryMethodMetadata.isVoid()) {
            sb.append(",?");
        }
        sb.append(")");
        sb.append("}");
        return sb.toString();
    }

    public RepositoryResult execute(Connection connection, RepositoryMethodMetadata repositoryMethodMetadata, List<RepositoryMethodPropertyValue> propertyValues) {

        try (CallableStatement callableStatement = connection.prepareCall(this.getRepositoryCall(repositoryMethodMetadata))) {
            for (RepositoryMethodPropertyValue property : propertyValues) {
                if (property.isNull()) {
                    callableStatement.setNull(property.getPropertyMetadata().getPropertyName(),
                            property.getPropertyMetadata().getJdbcType().getJdbcType().getVendorTypeNumber());
                } else {
                    callableStatement.setObject(property.getPropertyMetadata().getPropertyName(),
                            property.getValue(),
                            property.getPropertyMetadata().getJdbcType().getJdbcType().getVendorTypeNumber());
                }
            }
            callableStatement.execute();
            RepositoryResult result = new RepositoryResult(void.class, null);
            if (!repositoryMethodMetadata.isVoid()) {
                Object object = callableStatement.getObject(repositoryMethodMetadata.unwrap(Procedure.class).resultParameterName());
                result = new RepositoryResult(repositoryMethodMetadata.getResultClass(), object);
            }
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
