package org.example.jdbc.util;

import org.example.jdbc.util.action.FunctionRepositoryAction;
import org.example.jdbc.util.action.ProcedureRepositoryAction;
import org.example.jdbc.util.action.QueryRepositoryAction;
import org.example.jdbc.util.action.RepositoryAction;
import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyValue;

import javax.sql.DataSource;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Invocation handler that is used for calling proper {@link RepositoryAction}.
 * {@link RepositoryAction} is called based on the list of {@link RepositoryMethodMetadata} that is provided from the methods defined in the interface.
 * If method is not defined in any of the Repository annotations, it will return null.
 */
public class RepositoryProxyInvocationHandler implements InvocationHandler {

    private final Map<Method, RepositoryMethodMetadata> methodsMetadata;

    private static final List<RepositoryAction> repositoryActions = new ArrayList<>();
    private final DataSource dataSource;

    static {
        repositoryActions.add(new FunctionRepositoryAction());
        repositoryActions.add(new ProcedureRepositoryAction());
        repositoryActions.add(new QueryRepositoryAction());
    }

    public RepositoryProxyInvocationHandler(Map<Method, RepositoryMethodMetadata> methodActions, DataSource dataSource) {
        this.methodsMetadata = methodActions;
        this.dataSource = dataSource;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        RepositoryMethodMetadata methodMetadata = methodsMetadata.get(method);
        RepositoryAction repositoryAction = this.getRepositoryAction(methodMetadata);

        if (repositoryAction != null) {
            List<RepositoryMethodPropertyMetadata> propertyMetadata = methodMetadata.getProperties();
            List<RepositoryMethodPropertyValue> propertyValues = this.getMethodPropertyValues(propertyMetadata, args);
            RepositoryResult result = repositoryAction.execute(dataSource.getConnection(), methodMetadata, propertyValues);
            return result.getResult();
        }
        return null;
    }

    /**
     * Gets the {@link RepositoryAction} implementation based on the annotation that is set on the method.
     *
     * @param methodMetadata Metadata of the method that is being called.
     * @return Implementation of {@link RepositoryAction} if supported, otherwise null.
     */
    RepositoryAction getRepositoryAction(RepositoryMethodMetadata methodMetadata) {
        if (methodMetadata == null) {
            return null;
        }
        for (RepositoryAction repositoryAction : repositoryActions) {
            if (repositoryAction.isSupported(methodMetadata.getRepositoryAnnotation())) {
                return repositoryAction;
            }
        }
        return null;
    }

    /**
     * Gets the list of {@link RepositoryMethodPropertyValue} from the method that is being called.
     *
     * @param propertyMetadata List of {@link RepositoryMethodPropertyMetadata} that is defined from the method that is being called
     * @param args List of the values in proper order for the method that is being called.
     * @return List of {@link RepositoryMethodPropertyValue}
     */
    private List<RepositoryMethodPropertyValue> getMethodPropertyValues(List<RepositoryMethodPropertyMetadata> propertyMetadata, Object[] args) {
        List<RepositoryMethodPropertyValue> propertyValues = new ArrayList<>();
        if (args == null) {
            return propertyValues;
        }
        for (int i = 0; i < args.length; i++) {
            RepositoryMethodPropertyValue propertyValue = new RepositoryMethodPropertyValue(propertyMetadata.get(i), args[i]);
            propertyValues.add(propertyValue);
        }
        return propertyValues;
    }
}
