package org.example.jdbc.util.metadata;

/**
 * Class that holds Repository metadata for property in method.
 */
public class RepositoryMethodPropertyMetadata {

    private final String propertyName;
    private final Integer propertyIndex;
    private final Class propertyType;
    private final JDBCType jdbcType;

    public RepositoryMethodPropertyMetadata(String propertyName, Integer propertyIndex, Class propertyType) {
        this.propertyName = propertyName;
        this.propertyIndex = propertyIndex;
        this.propertyType = propertyType;
        this.jdbcType = JDBCType.getFromClass(propertyType);
    }

    public String getPropertyName() {
        return propertyName;
    }

    public Class getPropertyType() {
        return propertyType;
    }

    public Integer getPropertyIndex() {
        return propertyIndex;
    }

    public JDBCType getJdbcType() {
        return jdbcType;
    }
}
