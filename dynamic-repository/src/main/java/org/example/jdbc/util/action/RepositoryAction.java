package org.example.jdbc.util.action;

import org.example.jdbc.util.RepositoryResult;
import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyValue;

import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.util.List;

/**
 * Interface that represent repository action for given Repository Annotation.
 * Implementation of this interface defines proper action based on the annotation.
 */
public interface RepositoryAction {

    /**
     * Checks if the action is supported by given annotation.
     *
     * @param annotation {@link Annotation} that is set on repository method.
     * @return true if supported, otherwise false.
     */
    boolean isSupported(Annotation annotation);

    /**
     * Builds proper SQL call for given action
     *
     * @param repositoryMethodMetadata Method metadata
     * @return String of defined SQL call
     */
    String getRepositoryCall(RepositoryMethodMetadata repositoryMethodMetadata);

    /**
     * Executes the action.
     * Each action is implemented in implementation interface based on the annotation.
     *
     * @param connection {@link Connection} DB Connection that is used for executing proper action
     * @param repositoryMethodMetadata Repository method Metadata of the calling method
     * @param propertyValues List of Repository method arguments
     * @return {@link RepositoryResult}
     */
    RepositoryResult execute(Connection connection, RepositoryMethodMetadata repositoryMethodMetadata, List<RepositoryMethodPropertyValue> propertyValues);

    /**
     * Creates proper question marks for SQL call from the method arguments metadata.
     * @param properties List of method arguments metadata
     * @return SQL question marks from given arguments
     */
    default String getRepositoryCallParams(List<RepositoryMethodPropertyMetadata> properties) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0, j = 1; i < properties.size(); i++, j++) {
            sb.append("?");
            if (j != properties.size()) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
}
