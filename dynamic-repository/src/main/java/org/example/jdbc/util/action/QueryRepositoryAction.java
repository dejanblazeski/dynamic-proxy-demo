package org.example.jdbc.util.action;

import org.example.jdbc.util.RepositoryResult;
import org.example.jdbc.util.annotation.Query;
import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyValue;

import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class QueryRepositoryAction implements RepositoryAction {
    @Override
    public boolean isSupported(Annotation annotation) {
        return annotation != null && Query.class == annotation.annotationType();
    }

    @Override
    public String getRepositoryCall(RepositoryMethodMetadata repositoryMethodMetadata) {
        return null;
    }

    @Override
    public RepositoryResult execute(Connection connection, RepositoryMethodMetadata repositoryMethodMetadata, List<RepositoryMethodPropertyValue> propertyValues) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(repositoryMethodMetadata.unwrap(Query.class).value())) {
            for (RepositoryMethodPropertyValue property : propertyValues) {
                if (property.isNull()) {
                    preparedStatement.setNull(property.getPropertyMetadata().getPropertyIndex(),
                            property.getPropertyMetadata().getJdbcType().getJdbcType().getVendorTypeNumber());
                } else {
                    preparedStatement.setObject(property.getPropertyMetadata().getPropertyIndex(),
                            property.getValue(),
                            property.getPropertyMetadata().getJdbcType().getJdbcType().getVendorTypeNumber());
                }
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            RepositoryResult result = new RepositoryResult(void.class, null);
            if (!repositoryMethodMetadata.isVoid()) {
                Object object = null;
                if(resultSet.next()) {
                    object = resultSet.getObject(1);
                }
                result = new RepositoryResult(repositoryMethodMetadata.getResultClass(), object);
            }
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
