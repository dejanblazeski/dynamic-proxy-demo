package org.example.jdbc.util.metadata;

/**
 * Class that holds Repository method argument metadata and the value from the calling method
 */
public class RepositoryMethodPropertyValue {

    private final RepositoryMethodPropertyMetadata propertyMetadata;
    private final Object value;

    public RepositoryMethodPropertyValue(RepositoryMethodPropertyMetadata propertyMetadata, Object value) {
        this.propertyMetadata = propertyMetadata;
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public Boolean isNull() {
        return value == null;
    }

    public RepositoryMethodPropertyMetadata getPropertyMetadata() {
        return propertyMetadata;
    }
}
