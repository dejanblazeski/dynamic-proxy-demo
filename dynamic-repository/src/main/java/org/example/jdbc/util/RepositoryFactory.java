package org.example.jdbc.util;

import org.example.jdbc.util.metadata.RepositoryMethodMetadata;
import org.example.jdbc.util.metadata.RepositoryMethodPropertyMetadata;
import org.example.jdbc.util.reflection.RepositoryReflectionUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;

/**
 * Factory for creating Dynamic Repository Implementations.
 * Repository interfaces must implement {@link Repository} in order to be created.
 */
public class RepositoryFactory {

    private RepositoryFactory() {
    }

    /**
     * Creates implementation of interfaces that extends from {@link Repository}.
     * Implementation is based on Dynamic Proxy and as method handler is used {@link RepositoryProxyInvocationHandler}
     * Every method in provided interface is converted as list of {@link RepositoryMethodMetadata}
     * that will be used in invocation handler for calling proper {@link org.example.jdbc.util.action.RepositoryAction}
     *
     * @param repositoryInterface Interface that extends from {@link Repository}
     * @param <T> Interface that extends from {@link Repository}
     * @return Dynamic Proxy from provided interface
     */
    @SuppressWarnings("unchecked")
    public static <T extends Repository> T getInstance(Class<T> repositoryInterface) {
        Method[] methods = repositoryInterface.getMethods();
        Map<Method, RepositoryMethodMetadata> repositoryMethodsMetadata = getRepositoryMethodsMetadata(methods);
        return (T) Proxy.newProxyInstance(RepositoryFactory.class.getClassLoader(), new Class[]{repositoryInterface}, new RepositoryProxyInvocationHandler(repositoryMethodsMetadata, DataSourceUtil.getDataSource()));
    }

    /**
     * Creates List of {@link RepositoryMethodMetadata} for all methods defined in Repository Interface.
     *
     * @param methods list of {@link Method} from the Repository Interface
     * @return List of {@link RepositoryMethodMetadata} from given methods.
     */
    static Map<Method, RepositoryMethodMetadata> getRepositoryMethodsMetadata(Method[] methods) {
        Map<Method, RepositoryMethodMetadata> repositoryMethodsMetadata = new HashMap<>();

        for (Method method : methods) {
            RepositoryMethodMetadata repositoryMethodMetadata = RepositoryReflectionUtil.getRepositoryMethodMetadata(method);
            if (repositoryMethodMetadata != null) {
                repositoryMethodsMetadata.put(method, repositoryMethodMetadata);
            }
        }
        return repositoryMethodsMetadata;
    }
}
