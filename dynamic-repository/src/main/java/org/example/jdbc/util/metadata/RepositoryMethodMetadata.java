package org.example.jdbc.util.metadata;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * Class that holds metadata for Repository method.
 */
public class RepositoryMethodMetadata {

    private final List<RepositoryMethodPropertyMetadata> properties;
    private final Class resultClass;
    private final Annotation repositoryAnnotation;

    public RepositoryMethodMetadata(List<RepositoryMethodPropertyMetadata> properties,
                                    Class resultClass,
                                    Annotation repositoryAnnotation) {
        this.properties = properties;
        this.resultClass = resultClass;
        this.repositoryAnnotation = repositoryAnnotation;
    }


    public List<RepositoryMethodPropertyMetadata> getProperties() {
        return properties;
    }

    public Class getResultClass() {
        return resultClass;
    }

    public boolean isVoid() {
        return void.class.isAssignableFrom(resultClass);
    }

    public Annotation getRepositoryAnnotation() {
        return repositoryAnnotation;
    }

    @SuppressWarnings("unchecked")
    public <T> T unwrap(Class<T> repositoryAnnotation) {
        return (T) this.getRepositoryAnnotation();
    }
}
