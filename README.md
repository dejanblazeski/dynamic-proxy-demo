# Dynamic proxy examples

This repository contains 2 examples of using Dynamic Proxy

 - dynamic-logging-service - Simple service that logs every method before start and at the end. To run the example, use TestPersonApplication.java
 - dynamic-repository - Simple implementation of dynamic repository. 
 
## dynamic-repository

#### Intro

Dynamic repository is a simple implementation of dynamic repository. It uses Dynamic Proxy from Java 1.3 as technical approach of implementing repository interface.

To create dynamic repository, there is a simple example how to do it:

```
    public interface TestRepository extends Repository {
	    
	    @Query("select first_name from person where id = ?")
	    public String getFirstName(@Param(name = "id") Long id);

		@Procedure(procedureName = "test_procedure")
		public void performProcedureAction(@Param(name = "firstname") String firstName);
    }
```

The interface must extends from Repository.java.

To mark method as DB action, use one of the annotations:

 - @Procedure - to call DB procedure - if you want to return OUT parameter, use the annotation argument resultParameterName which must corresponds with OUT parameter name from the procedure.
 - @Query - to execute query from DB

To pass parameters to query or procedure, use @Param annotation in method argument.
